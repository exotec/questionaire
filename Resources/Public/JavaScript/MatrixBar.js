define(['TYPO3/CMS/Questionaire/chart'], function(Chart) {

    var question = 'Matrix Question Title';
    var answerLabels = ["++","+",".","-","--"];
    var data = [
        {
            label: "Frage 1",
            backgroundColor: "#9CFBF6",
            data: [3,4,3,2,1]
        }, {
            label: "Frage 2",
            backgroundColor: "#2966ff",
            data: [6,4,3,4,5]
        }
        ,{
            label: "Frage 3",
            backgroundColor: "#c470b7",
            data: [2,4,3,2,3]
        }
        ,{
            label: "Frage 4",
            backgroundColor: "#56c4bf",
            data: [4,3,5,6,7]
        }
        ,{
            label: "Frage 5",
            backgroundColor: "#64cd63",
            data: [4,3,2,1,2]
        }
        ,{
            label: "Frage 6",
            backgroundColor: "#ba9041",
            data: [3,2,3,4,4]
        }
        ,{
            label: "Frage 7",
            backgroundColor: "#91a25a",
            data: [2,3,4,3,4]
        }
        ,{
            label: "Frage 8",
            backgroundColor: "#9096c4",
            data: [4,3,2,1,3]
        }
    ];


    for(i in matrixquestions) {



        new Chart(document.getElementById("matrixbar-chart_"+matrixquestions[i].uid), {
            type: 'bar',
            data: {
                labels: answerLabels,
                datasets: data
            },
            options: {
                title: {
                    display: true,
                    text: question
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            stepSize: 1,
                            min: 0
                        }
                    }],
                }
            }
        });


    }

});