define(['TYPO3/CMS/Questionaire/chart'], function(Chart) {

    var question = 'Matrix Question Title';
    var labels = ["Frage 1", "Frage 2", "Frage 3", "Frage 4", "Frage 5", "Frage 6", "Frage 7", "Frage 8"];
    var answerLabels = ["++","+",".","-","--"];
    var data = [
        {
            label: "Frage 1",
            fill: true,
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "#9CFBF6",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(179,181,198,1)",
            data: [3,4,3,2,1]
        }, {
            label: "Frage 2",
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "#2966ff",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(179,181,198,1)",
            data: [6,4,3,4,5]
        }
        ,{
            label: "Frage 3",
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "#c470b7",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(179,181,198,1)",
            data: [2,4,3,2,3]
        }
        ,{
            label: "Frage 4",
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "#56c4bf",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(179,181,198,1)",
            data: [4,3,5,6,7]
        }
        ,{
            label: "Frage 5",
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "#64cd63",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(179,181,198,1)",
            data: [4,3,2,1,2]
        }
        ,{
            label: "Frage 6",
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "#ba9041",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(179,181,198,1)",
            data: [3,2,3,4,4]
        }
        ,{
            label: "Frage 7",
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "#91a25a)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(179,181,198,1)",
            data: [2,3,4,3,4]
        }
        ,{
            label: "Frage 8",
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "#9096c4",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(179,181,198,1)",
            data: [4,3,2,1,3]
        }
    ];

    for(i in matrixquestions) {



        new Chart(document.getElementById("matrixradar-chart_"+matrixquestions[i].uid), {
            type: 'radar',
            data: {
                labels: answerLabels,
                datasets: data
            },
            options: {
                title: {
                    display: true,
                    text: question
                }
            },
            hover: {
                mode: 'dataset'
            },
        });


    }

});