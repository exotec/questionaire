define(['TYPO3/CMS/Questionaire/chart'], function(Chart) {

    for(i in questions) {
        var labels = [];
        var data = [];
        for(x in answers) {
            if(answers[x].question == questions[i].uid) {
                labels.push(answers[x].total+"x "+answers[x].answer);
                data.push(answers[x].total);
            }
        }

        new Chart(document.getElementById('doughnut-chart_' + questions[i].uid), {
          type: 'doughnut',
          data: {
            labels: labels,
            datasets: [
              {
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: data
              }
            ]
          },
          options: {
              responsive: true,
              title: {
                display: true,
                text: questions[i].title
              },
              tooltips: {
                  callbacks: {
                      // this callback is used to create the tooltip label
                      label: function(tooltipItem, data) {
                          // get the data label and data value to display
                          // convert the data value to local string so it uses a comma seperated number
                          var dataLabel = data.labels[tooltipItem.index];
                          // var value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString();
                          var value = '';

                          // make this isn't a multi-line label (e.g. [["label 1 - line 1, "line 2, ], [etc...]])
                          if (Chart.helpers.isArray(dataLabel)) {
                              // show value on first line of multiline label
                              // need to clone because we are changing the value
                              dataLabel = dataLabel.slice();
                              dataLabel[0] += value;
                          } else {
                              dataLabel += value;
                          }

                          // return the text to display on the tooltip
                          return dataLabel;
                      }
                  }
              }
          }
        });
    }

    require(['jquery'], function ($) {
        $(".openNextDonut").click(function (e) {
            var text = $(this).text();
            if(text == 'Anzeigen') {
                $(this).text('Schliessen');
            } else {
                $(this).text('Anzeigen');
            }
            e.preventDefault();
            var next = $(this).closest('div.charts').find(".donutChart");
            next.toggle();
        });
    });

});