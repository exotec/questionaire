define(['TYPO3/CMS/Questionaire/chart', 'TYPO3/CMS/Questionaire/ChartPluginAnnotation'], function(Chart, annotation) {

    var daysLabel = JScountDays+" Tagen";
    if(JScountDays == '1') {
        daysLabel = 'Heute';
    }

    var data = {
            labels: dates,
            datasets: [
                {
                    lineTension: 0.2,
                    data: closedInvitations,
                    label: "Beendete Umfragen",
                    pointRadius: 6,
                    pointHoverRadius: 8,
                    borderWidth: 2,
                    pointStyle: 'circle',
                    pointBackgroundColor: "#64cd63",
                    pointBorderColor: '#fff',
                    backgroundColor: "rgba(100,205,99,0.05)",
                    borderColor: "#64cd63",
                },
                {
                    lineTension: 0.2,
                    data: openInvitations,
                    label: "Nicht gefolgten Einladungen",
                    pointRadius: JSpointSizeOpen,
                    pointHoverRadius: JSpointSizeOpen,
                    borderWidth: 2,
                    pointStyle: 'triangle',
                    pointRotation: -90,
                    pointBackgroundColor: "#0799f5",
                    pointBorderColor: '#fff',
                    backgroundColor: "transparent",
                    borderColor: "transparent",
                },
                {
                    lineTension: 0.2,
                    data: startedInvitations,
                    label: "Nicht beendete Umfragen",
                    pointRadius: 6,
                    pointHoverRadius: 8,
                    borderWidth: 2,
                    pointStyle: 'crossRot',
                    pointBackgroundColor: "#ba2f38",
                    pointBorderColor: '#ba2f38',
                    backgroundColor: "rgba(186,47,56,0.05)",
                    borderColor: "#ba2f38",
                }
                ]
        };

    var options = {
        title: {
            display: true,
            text: ' Legende klicken zum Filtern'
        },
        tooltips: {
            enabled: true,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        legend: {
            labels: {
                filter: function(item, chart) {
                    // Remove a specific legend
                    return !item.text.includes('Nicht gefolgten Einladungen');
                }
            }
        },
        // label ungefolgten Einladungen
        annotation: {
            annotations: [{
                type: 'line',
                mode: 'horizontal',
                scaleID: 'y-axis-1',
                value: JScountOpen,
                borderColor: 'transparent',
                borderWidth: 2,
                label: {
                    enabled: true,
                    backgroundColor: '#0799f5',
                    fontFamily: "sans-serif",
                    fontSize: 11,
                    fontStyle: "normal",
                    fontColor: "#fff",
                    xPadding: 6,
                    yPadding: 6,
                    cornerRadius: 3,
                    position: "right",
                    xAdjust: 12,
                    yAdjust: 0,
                    content: "Nicht gefolgten Einladungen ("+JScountOpen+")"
                },
            }]
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: "Läuft seit "+daysLabel,
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Einladungen'
                },
                type: 'linear',
                display: true,
                position: 'left',
                id: 'y-axis-1',
                ticks: {
                    min: 0,
                    max: allInvitations,
                    stepSize: 1
                }
            }, {
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Einladungen'
                },
                type: 'linear',
                display: true,
                position: 'right',
                id: 'y-axis-2',
                ticks: {
                    min: 0,
                    max: allInvitations,
                    stepSize: 1
                }
            }],
        }
    };


    new Chart(document.getElementById("line-chart"), {
        type: 'line',
        data: data,
        options: options
    });


});