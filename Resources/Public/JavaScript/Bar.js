define(['TYPO3/CMS/Questionaire/chart'], function(Chart) {

    for(i in questions) {
        var labels = [];
        var data = [];
        for(x in answers) {
            if(answers[x].question == questions[i].uid) {
                labels.push(answers[x].total+"x "+answers[x].answer);
                data.push(answers[x].total);
            }
        }

        // Bar chart
        new Chart(document.getElementById('bar-chart_' + questions[i].uid), {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [
                    {
                        label: " ",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#cccccc"],
                        data: data
                    }
                ]
            },
            options: {
                legend: { display: false },
                tooltips: {
                    callbacks: {
                        // use label callback to return the desired label
                        label: function(tooltipItem, data) {
                            return tooltipItem.xLabel;
                        },
                        // remove title
                        title: function(tooltipItem, data) {
                            return;
                        }
                    }
                },
                title: {
                    display: true,
                    text: questions[i].title
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            stepSize: 1,
                            min: 0
                        }
                    }],
                }
            }
        });
    }

});