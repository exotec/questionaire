<?php
/**
 * Copyright (c) 2018.  Alexander Weber <weber@exotec.de> - exotec - TYPO3 Services
 *
 * All rights reserved
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 */

namespace EXOTEC\Questionaire\Task;

use EXOTEC\Questionaire\Domain\Repository\FrontendUserRepository;
use EXOTEC\Questionaire\Domain\Repository\SurveyRepository;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

/**
 * Additional fields provider class for usage with the Scheduler's test task
 */
class InviteAdditionalFieldProvider implements \TYPO3\CMS\Scheduler\AdditionalFieldProviderInterface
{
    /**
     * @param array $taskInfo Reference to the array containing the info used in the add/edit form
     * @param AbstractTask|NULL $task When editing, reference to the current task. NULL when adding.
     * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
     * @return array Array containing all the information pertaining to the additional fields
     */
    public function getAdditionalFields(array &$taskInfo, $task, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject)
    {

        /** @var ObjectManager $this->objectManager */
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var SurveyRepository $this->frontendUserRepository */
        $this->surveyRepository = $this->objectManager->get(SurveyRepository::class);
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get(Typo3QuerySettings::class);
        $querySettings->setRespectStoragePage(FALSE);
        $this->surveyRepository->setDefaultQuerySettings($querySettings);
        $surveys = $this->surveyRepository->findAll();

        $fieldCode_surveys = '<select class="form-control" name="tx_scheduler[surveys]"><option value="0">-- bitte wählen --</option>';
        foreach ($surveys as $survey) {
            if($survey->getUid() == $task->surveys) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $fieldCode_surveys .= '<option '.$selected.' value="' . $survey->getUid() . '">'.$survey->getTitle().'</option>';
        }
        $fieldCode_surveys .= '<select>';
        $additionalFields['surveys'] = [
            'code' => $fieldCode_surveys,
            'label' => 'Umfrage wählen:',
            'cshKey' => '_MOD_system_txschedulerM1',
            'cshLabel' => 'surveys'
        ];

        return $additionalFields;
    }

    /**
     * This method checks any additional data that is relevant to the specific task
     * If the task class is not relevant, the method is expected to return TRUE
     *
     * @param array	 $submittedData Reference to the array containing the data submitted by the user
     * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
     * @return bool TRUE if validation was ok (or selected class is not relevant), FALSE otherwise
     */
    public function validateAdditionalFields(array &$submittedData, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject)
    {
        $submittedData['surveys'] = trim($submittedData['surveys']);
        if ( !empty($submittedData['surveys']) && $submittedData['surveys'] != '0') {
            $result = true;
            $this->showMessage('','Einladungen werden für die Umfrage mit der UID='.$submittedData['surveys'].' versendet',\TYPO3\CMS\Core\Messaging\FlashMessage::INFO);
        }  else {
            $result = false;
            $this->showMessage('','Keine Umfrage ausgewählt!',\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
        }
        return $result;
    }

    /**
     * This method is used to save any additional input into the current task object
     * if the task class matches
     *
     * @param array $submittedData Array containing the data submitted by the user
     * @param \TYPO3\CMS\Scheduler\Task\AbstractTask $task Reference to the current task object
     * @return void
     */
    public function saveAdditionalFields(array $submittedData, \TYPO3\CMS\Scheduler\Task\AbstractTask $task)
    {
        $task->surveys = $submittedData['surveys'];
    }

    /**
     * @return object
     */
    public function showMessage($header, $text, $severity)
    {
        $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessage::class,
            $text,
            $header, // [optional] the header
            $severity, // [optional] the severity defaults to \TYPO3\CMS\Core\Messaging\FlashMessage::OK
            true // [optional] whether the message should be stored in the session or only in the \TYPO3\CMS\Core\Messaging\FlashMessageQueue object (default is false)
        );
        /** @var ObjectManager $this->objectManager */
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        $flashMessageService = $this->objectManager->get(FlashMessageService::class);
        $messageQueue = $flashMessageService->getMessageQueueByIdentifier();
        $messageQueue->addMessage($message);
        return $message;
    }
}
