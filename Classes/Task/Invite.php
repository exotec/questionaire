<?php
namespace EXOTEC\Questionaire\Task;

use Doctrine\Common\Util\Debug;
use EXOTEC\Questionaire\Domain\Model\FrontendUser;
use EXOTEC\Questionaire\Domain\Model\Invitation;
use EXOTEC\Questionaire\Domain\Repository\FrontendUserRepository;
use EXOTEC\Questionaire\Domain\Repository\InvitationRepository;
use EXOTEC\Questionaire\Utility\Crypt;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

class Invite extends \TYPO3\CMS\Scheduler\Task\AbstractTask {

    public function execute() {

        /** @var ObjectManager $this->objectManager */
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var FrontendUserRepository $this->frontendUserRepository */
        $this->frontendUserRepository = $this->objectManager->get(FrontendUserRepository::class);

        /** @var InvitationRepository $this->invitationRepository */
        $this->invitationRepository = $this->objectManager->get(InvitationRepository::class);

        /** @var PersistenceManager $this */
        $this->persistenceManager = $this->objectManager->get(PersistenceManager::class);

        /** @var ConfigurationManager $this */
        $this->configurationManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

        /** @var ConfigurationManagerInterface $extbaseFrameworkConfiguration */
        $extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        $this->settings = $extbaseFrameworkConfiguration['plugin.']['tx_questionaire_pi1.']['settings.'];
        $this->view = $extbaseFrameworkConfiguration['plugin.']['tx_questionaire_pi1.']['view.'];
        $this->config = $extbaseFrameworkConfiguration['config.'];

        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get(Typo3QuerySettings::class);
        $querySettings->setRespectStoragePage(FALSE);

        $this->frontendUserRepository->setDefaultQuerySettings($querySettings);
        /** @var FrontendUser $feUsers */
        $feUsers = $this->frontendUserRepository->findByPid($this->settings['feuserPid']);

        /** @var FrontendUser $user */
        foreach ($feUsers as $user) {
            $this->invitationRepository->setDefaultQuerySettings($querySettings);
            /** @var Invitation $existingInvitation */
            $existingInvitation = $this->invitationRepository->findByFeUserAndSurvey($user->getUid(), $this->surveys);

            // check if an invitation for the given survey already was send
            if( is_array($existingInvitation) && in_array($this->surveys, $existingInvitation)) {
                $this->showMessage(
                    '',
                    'Skipped: '.$user->getUsername().' ('.$user->getEmail().') - wurde bereits eingeladen',
                    \TYPO3\CMS\Core\Messaging\FlashMessage::INFO);
            } elseif ( !$this->validateEmail($user->getEmail()) ) {
                $this->showMessage(
                    '',
                    'Invalid Email: '.$user->getEmail().' ('.$user->getUsername().')',
                    \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
            } else {
                $res[] = $this->sendMail($user);
            }
        }

        if( is_array($res) && in_array( false, $res) ) {
            return false;
        }

        return true;
    }


    /**
     * @param $email
     * @return bool
     */
    public function validateEmail($email){
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }


    /**
     * @param $user FrontendUser
     * @return bool
     */
    public function sendMail(FrontendUser $user) {
        if( !$user->getEmail() ) {
            $this->showMessage(
                '',
                'Executed: '.$user->getUsername().' - keine Email gefunden',
                \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
            return true;

        } else {
            $templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('EXT:questionaire/Resources/Private/Templates/');
            $templateLayoutRootPath = $this->view['layoutRootPaths.'];
            $templatePathAndFilename = $templateRootPath . 'Invite/Email.html';

            /** @var StandaloneView $view */
            $emailView = $this->objectManager->get(StandaloneView::class);
            $emailView->setLayoutRootPaths($templateLayoutRootPath);
            $emailView->setTemplatePathAndFilename($templatePathAndFilename);

            $emailSubject = LocalizationUtility::translate('invite.email.subject','Questionaire');

            // set invatation UID
            $data['invitation_uid'] = $this->writeInvitation($user);

            // set survey UID
            $data['invited2Survey'] = $this->surveys;
            foreach ($data as $name => $value) {
                $parameter .= '&'.$name.'='.$value;
            }
            $link = $this->buildLink($parameter);

            $emailView->assign('link', $link);
            $emailView->assign('anrede', 'LocalizationUtility::translate(label.gender.$user->getGender(),Questionaire)');
            $emailView->assign('first_name', $user->getFirstName());
            $emailView->assign('last_name', $user->getLastName());
            $emailView->assign("subject", $emailSubject);
            $emailBody = $emailView->render();

            if($user instanceof FrontendUser) {
                $message = GeneralUtility::makeInstance(MailMessage::class);
                $message->setSubject(LocalizationUtility::translate('invite.email.subject','Questionaire'))
                    ->setFrom(array($this->settings['adminEmailFrom'] => $this->settings['adminEmailFromName']))
                    ->setTo(array($user->getEmail()))
                    ->html($emailBody,'text/plain');

                if($message->send()) {
                    $this->showMessage(
                        '',
                        'Executed: '.$user->getUsername().' - '.$user->getEmail().'',
                        \TYPO3\CMS\Core\Messaging\FlashMessage::OK);
                    return true;
                }

                return false;
            }
        }
    }


    /**
     * writes the new invitation and returns the inviteation UID which we need in the link &key param
     *
     * @param $user FrontendUser
     * @return int
     */
    public function writeInvitation($user)
    {
        /** @var Invitation $newInvitation */
        $newInvitation = new Invitation();
        $newInvitation->setFeUser($user->getUid());
        $newInvitation->setResult(0);
        $newInvitation->setSurvey($this->surveys);
        $newInvitation->setPid($this->settings['storagePid']);

        $this->invitationRepository->add($newInvitation);
        $this->persistenceManager->persistAll();
        return $newInvitation->getUid();
    }


    /**
     * @param $parameter

     * @return string
     */
    public function buildLink($parameter)
    {
        /** @var Crypt $crypt */
        $crypt = $this->objectManager->get(Crypt::class);
        $encryptedParams = $crypt->encryptGetParams($parameter);

        parse_str($parameter, $parameterArray);
        $args = ['tx_questionaire_pi1' => ['survey' => $parameterArray['invited2Survey'], 'action' => 'show', 'key' => $encryptedParams]];

        $uriBuilder = $this->objectManager->get(\TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder::class);
        $redirectUrl = $uriBuilder->reset()
            ->setTargetPageUid($this->settings['listPid'])
            ->setLinkAccessRestrictedPages(true)
            ->setArguments($args)
            ->buildFrontendUri();

        $link = $uriBuilder->reset()
            ->setTargetPageUid($this->settings['loginPid'])
            ->setLinkAccessRestrictedPages(true)
            ->setArguments(['redirect_url' => $redirectUrl])
            ->buildFrontendUri();

        return $link;
    }

    /**
     * @return object
     */
    public function showMessage($header, $text, $severity)
    {
        $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessage::class,
            $text,
            $header, // [optional] the header
            $severity, // [optional] the severity defaults to \TYPO3\CMS\Core\Messaging\FlashMessage::OK
            true // [optional] whether the message should be stored in the session or only in the \TYPO3\CMS\Core\Messaging\FlashMessageQueue object (default is false)
        );
        $flashMessageService = $this->objectManager->get(FlashMessageService::class);
        $messageQueue = $flashMessageService->getMessageQueueByIdentifier();
        $messageQueue->addMessage($message);
        return $message;
    }

    /**
     * This method is designed to return some additional information about the task,
     * that may help to set it apart from other tasks from the same class
     * This additional information is used - for example - in the Scheduler's BE module
     * This method should be implemented in most task classes
     *
     * @return string Information to display
     */
    public function getAdditionalInformation()
    {
        return 'Umfrage=' . $this->surveys;
    }

}
