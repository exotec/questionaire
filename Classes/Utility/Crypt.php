<?php
namespace EXOTEC\Questionaire\Utility;

class Crypt {

    /**
     * @param $keyArgs
     * @return string
     */
    public function encryptGetParams ($keyArgs)
    {
        $urlenc = rawurlencode($keyArgs);
        $gzdeflate = gzdeflate($urlenc);
        $hash = base64_encode($gzdeflate);
        return $hash;
    }

    /**
     * @param $hash
     * @return string
     */
    public function decryptGetParams ($hash)
    {
        $base64_decode = base64_decode($hash);
        $gzinflate = gzinflate($base64_decode);
        $params = rawurldecode($gzinflate);
        return $params;
    }
}