<?php
namespace EXOTEC\Questionaire\Domain\Repository;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

/**
 * The repository for ResultNotes
 */
class ResultNoteRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
