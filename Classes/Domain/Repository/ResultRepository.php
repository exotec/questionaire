<?php
namespace EXOTEC\Questionaire\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

/**
 * The repository for Results
 */
class ResultRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * frontendUserRepository
     *
     * @var FrontendUserRepository
     */
    protected $frontendUserRepository = null;

    /**
     * @param FrontendUserRepository $frontendUserRepository
     */
    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }


    public function findByEmail($email)
    {
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectSysLanguage(FALSE);
        $querySettings->setRespectStoragePage(FALSE);
        $this->frontendUserRepository->setDefaultQuerySettings($querySettings);

        $feUsers = $this->frontendUserRepository->findByEmail($email);

        foreach ($feUsers as $index => $feUser) {
            $invitations[] = $this->findByFeUser($feUser);
        }
        return $invitations;
    }


    public function findAllByDomainOfEmail($email)
    {

        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectSysLanguage(FALSE);
        $querySettings->setRespectStoragePage(FALSE);
        $this->frontendUserRepository->setDefaultQuerySettings($querySettings);
        $this->setDefaultQuerySettings($querySettings);

        $feUsers = $this->frontendUserRepository->findByDomainOfEmail($email);
        foreach ($feUsers as $index => $feUser) {
            $invitations[] = $this->findByFeUser($feUser);
        }
        return $invitations;
    }


    public function findBySurveyAndLang($survey,$lang=0)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                [
                    $query->equals('survey', $survey),
                    $query->equals('sysLanguageUid', $lang)
                ]
            )
        );
        return $query->execute();
    }



//    public function countAllResultsByLang($lang)
//    {
//
//        $table = 'tx_questionaire_domain_model_result';
//        /** @var QueryBuilder $queryBuilder */
//        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
//            ->getQueryBuilderForTable($table);
//        $data = $queryBuilder
//            ->select('*')
//            ->from($table)
//            ->where($queryBuilder->expr()->eq('sys_language_uid', $lang))
//            ->execute()
//            ->fetchAll();
//
//        return count($data);
//    }

    public function countAllInvitationsByLang($lang)
    {

        $table = 'tx_questionaire_domain_model_invitation';
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($table);
        $data = $queryBuilder
            ->select('*')
            ->from($table)
            ->where(
                $queryBuilder->expr()->eq('sys_language_uid', $lang),
                $queryBuilder->expr()->eq('survey_completed', 1)
            )
            ->execute()
            ->fetchAll();

        return count($data);
    }

}
