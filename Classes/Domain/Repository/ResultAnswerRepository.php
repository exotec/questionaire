<?php
namespace EXOTEC\Questionaire\Domain\Repository;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

/**
 * The repository for ResultAnswers
 */
class ResultAnswerRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @param integer $uid
     */
    public function countMatrixResults($answer, $question)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                [
                    $query->equals('answer', $answer),
                    $query->equals('question', $question),
                    $query->like('type', 'Matrix')
                ]
            )
        );
        return $query->execute();
    }

    /**
     * @param integer $uid
     */
    public function findByAnswerAndQuestion($answer, $question, $type)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                [
                    $query->equals('answer', $answer),
                    $query->equals('question', $question),
                    $query->like('type', $type)
                ]
            )
        );
        return $query->execute();
    }
}
