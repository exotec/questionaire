<?php
namespace EXOTEC\Questionaire\Domain\Repository;

use Doctrine\DBAL\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

/**
 * The repository for Invitations
 */
class InvitationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    public function findAllByLang($lang)
    {

        $table = 'tx_questionaire_domain_model_invitation';
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($table);
        $data = $queryBuilder
            ->select('*')
            ->from($table)
            ->where($queryBuilder->expr()->eq('sys_language_uid', $lang))
            ->execute()
            ->fetchAll();

        return count($data);
    }

    public function findOpenInvitationsBySurvey($survey)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                [
                    $query->equals('surveyStarted', 0),
                    $query->equals('survey', $survey)
                ]
            )
        );
        return $query->execute();
    }

//    public function findOpenInvitationsBySurvey($survey)
//    {
//        $table = 'tx_questionaire_domain_model_invitation';
//        /** @var QueryBuilder $queryBuilder */
//        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
//            ->getQueryBuilderForTable($table);
//        $result = $queryBuilder
//            ->select('survey')
//            ->from($table)
//            ->where(
//                $queryBuilder->expr()->eq('deleted', 0)
//            )->andWhere(
//                $queryBuilder->expr()->eq('survey', $survey),
//                $queryBuilder->expr()->eq('survey_started', 0),
//                $queryBuilder->expr()->eq('hidden', 0)
//            )
//            ->execute()
//            ->fetchAll();
//
//        return $result;
//    }

    public function findCompletedByFeUserAndSurvey($user, $survey)
    {
        $table = 'tx_questionaire_domain_model_invitation';
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($table);
        $surveyUids = $queryBuilder
            ->select('survey')
            ->from($table)
            ->where(
                $queryBuilder->expr()->eq('deleted', 0)
            )->andWhere(
                $queryBuilder->expr()->eq('fe_user', $user),
                $queryBuilder->expr()->eq('survey', $survey),
                $queryBuilder->expr()->eq('survey_completed', 1),
                $queryBuilder->expr()->eq('hidden', 0)
            )
            ->execute()
            ->fetchAll();

        foreach ($surveyUids as $item) {
            $result[] = $item['survey'];
        }

        return $result;
    }

    public function findByFeUserAndSurvey($user, $survey)
    {
        $table = 'tx_questionaire_domain_model_invitation';
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($table);
        $surveyUids = $queryBuilder
            ->select('survey')
            ->from($table)
            ->where(
                $queryBuilder->expr()->eq('deleted', 0)
            )->andWhere(
                $queryBuilder->expr()->eq('fe_user', $user),
                $queryBuilder->expr()->eq('survey', $survey),
                $queryBuilder->expr()->eq('hidden', 0)
            )
            ->execute()
            ->fetchAll();

        foreach ($surveyUids as $item) {
            $result[] = $item['survey'];
        }

        return $result;
    }

    public function findClosedInvitationsBySurvey($survey)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                [
                    $query->equals('surveyCompleted', 1),
                    $query->equals('survey', $survey),
                    $query->greaterThanOrEqual('sysLanguageUid', 0)
                ]
            )
        );
        return $query->execute();
    }

    public function findCanceledInvitationsBySurvey($survey)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                [
                    $query->equals('surveyStarted', 1),
                    $query->equals('surveyCompleted', 0),
                    $query->equals('survey', $survey),
                    $query->greaterThanOrEqual('sysLanguageUid', 0)
                ]
            )
        );
        return $query->execute();
    }

    public function findClosedByDateInRange($crdate, $day)
    {
            $query = $this->createQuery();
            $query->matching(
                $query->logicalAnd(
                    [
                        $query->greaterThanOrEqual('surveyCompletedDate', $crdate),
                        $query->lessThanOrEqual('surveyCompletedDate', $day),
                        $query->equals('surveyCompleted', 1)
                    ]
                )
            );
            return $query->execute();
    }


    public function findOpenByDateInRange($crdate, $day)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                [
                    $query->greaterThanOrEqual('crdate', $crdate),
                    $query->lessThanOrEqual('crdate', $day),
                    $query->equals('surveyCompleted', 0),
                    $query->equals('surveyStarted', 0),
                ]
            )
        );
        return $query->execute();
    }

//    public function findFirstDayOpenByDateInRange($crdate, $day)
//    {
//        $query = $this->createQuery();
//        $query->matching(
//            $query->logicalAnd(
//                [
//                    $query->greaterThanOrEqual('crdate', $crdate),
//                    $query->lessThanOrEqual('crdate', $day)
//                ]
//            )
//        );
//        return $query->execute();
//    }


    public function findStartedByDateInRange($date, $date2)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                [
                    $query->greaterThanOrEqual('surveyStartedDate', $date),
                    $query->lessThanOrEqual('surveyStartedDate', $date2),
                    $query->equals('surveyStarted', 1),
                    $query->equals('surveyCompleted', 0)
                ]
            )
        );
        return $query->execute();
    }


}



