<?php
namespace EXOTEC\Questionaire\Domain\Model;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

/**
 * Question
 */
class Question extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * type
     *
     * @var string
     */
    protected $type = '';

    /**
     * htmlElement
     *
     * @var string
     */
    protected $htmlElement = '';

    /**
     * minAnswers
     *
     * @var int
     */
    protected $minAnswers = 0;

    /**
     * maxAnswers
     *
     * @var int
     */
    protected $maxAnswers = 0;

    /**
     * answers
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\Answer>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $answers = null;

    /**
     * matrixquestions
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\Matrixquestion>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $matrixquestions = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->answers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->matrixquestions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }



    /**
     * Returns the type
     *
     * @return int $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param int $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getHtmlElement()
    {
        return $this->htmlElement;
    }

    /**
     * @param string $htmlElement
     */
    public function setHtmlElement($htmlElement)
    {
        $this->htmlElement = $htmlElement;
    }

    /**
     * @return int
     */
    public function getMinAnswers ()
    {
        return $this->minAnswers;
    }

    /**
     * @param int $minAnswers
     */
    public function setMinAnswers ($minAnswers)
    {
        $this->minAnswers = $minAnswers;
    }

    /**
     * @return int
     */
    public function getMaxAnswers()
    {
        return $this->maxAnswers;
    }

    /**
     * @param int $maxAnswers
     */
    public function setMaxAnswers($maxAnswers)
    {
        $this->maxAnswers = $maxAnswers;
    }





    /**
     * Adds a Answer
     *
     * @param \EXOTEC\Questionaire\Domain\Model\Answer $answer
     * @return void
     */
    public function addAnswer(\EXOTEC\Questionaire\Domain\Model\Answer $answer)
    {
        $this->answers->attach($answer);
    }

    /**
     * Removes a Answer
     *
     * @param \EXOTEC\Questionaire\Domain\Model\Answer $answerToRemove The Answer to be removed
     * @return void
     */
    public function removeAnswer(\EXOTEC\Questionaire\Domain\Model\Answer $answerToRemove)
    {
        $this->answers->detach($answerToRemove);
    }

    /**
     * Returns the answers
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\Answer> $answers
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Sets the answers
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\Answer> $answers
     * @return void
     */
    public function setAnswers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $answers)
    {
        $this->answers = $answers;
    }

    /**
     * Adds a Matrixquestion
     *
     * @param \EXOTEC\Questionaire\Domain\Model\Matrixquestion $matrixquestion
     * @return void
     */
    public function addMatrixquestion(\EXOTEC\Questionaire\Domain\Model\Matrixquestion $matrixquestion)
    {
        $this->matrixquestions->attach($matrixquestion);
    }

    /**
     * Removes a Matrixquestion
     *
     * @param \EXOTEC\Questionaire\Domain\Model\Matrixquestion $matrixquestionToRemove The Matrixquestion to be removed
     * @return void
     */
    public function removeMatrixquestion(\EXOTEC\Questionaire\Domain\Model\Matrixquestion $matrixquestionToRemove)
    {
        $this->matrixquestions->detach($matrixquestionToRemove);
    }

    /**
     * Returns the matrixquestions
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\Matrixquestion> $matrixquestions
     */
    public function getMatrixquestions()
    {
        return $this->matrixquestions;
    }

    /**
     * Sets the matrixquestions
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\Matrixquestion> $matrixquestions
     * @return void
     */
    public function setMatrixquestions(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $matrixquestions)
    {
        $this->matrixquestions = $matrixquestions;
    }

    /**
     * @return int
     */
    public function getLanguageUid()
    {
        return $this->_languageUid;
    }

    /**
     * @param int $languageUid
     */
    public function setLanguageUid($languageUid)
    {
        $this->_languageUid = $languageUid;
    }


}
