<?php
namespace EXOTEC\Questionaire\Domain\Model;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/


/**
 * Result
 */
class Result extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * $languageUid
     *
     * @var int

     */
    protected $languageUid = 0;

    /**
     * survey
     *
     * @var int
     */
    protected $survey = 0;

    /**
     * invitation
     *
     * @var int
     */
    protected $invitation = 0;

    /**
     * feUser
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $feUser = 0;

    /**
     * answers
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\ResultAnswer>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $answers = null;

    /**
     * notes
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\ResultNote>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $notes = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->answers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->notes = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    public function getFeUser ()
    {
        return $this->feUser;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $feUser
     */
    public function setFeUser ($feUser)
    {
        $this->feUser = $feUser;
    }



    /**
     * @return int
     */
    public function getSurvey ()
    {
        return $this->survey;
    }

    /**
     * @param int $survey
     */
    public function setSurvey ($survey)
    {
        $this->survey = $survey;
    }

    /**
     * @return int
     */
    public function getInvitation()
    {
        return $this->invitation;
    }

    /**
     * @param int $invitation
     */
    public function setInvitation($invitation)
    {
        $this->invitation = $invitation;
    }



    /**
     * Adds a ResultAnswer
     *
     * @param \EXOTEC\Questionaire\Domain\Model\ResultAnswer $answer
     * @return void
     */
    public function addAnswer(\EXOTEC\Questionaire\Domain\Model\ResultAnswer $answer)
    {
        $this->answers->attach($answer);
    }

    /**
     * Removes a ResultAnswer
     *
     * @param \EXOTEC\Questionaire\Domain\Model\ResultAnswer $answerToRemove The ResultAnswer to be removed
     * @return void
     */
    public function removeAnswer(\EXOTEC\Questionaire\Domain\Model\ResultAnswer $answerToRemove)
    {
        $this->answers->detach($answerToRemove);
    }

    /**
     * Returns the answers
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\ResultAnswer> $answers
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Sets the answers
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\ResultAnswer> $answers
     * @return void
     */
    public function setAnswers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $answers)
    {
        $this->answers = $answers;
    }

    /**
     * Adds a ResultNote
     *
     * @param \EXOTEC\Questionaire\Domain\Model\ResultNote $note
     * @return void
     */
    public function addNote(\EXOTEC\Questionaire\Domain\Model\ResultNote $note)
    {
        $this->notes->attach($note);
    }

    /**
     * Removes a ResultNote
     *
     * @param \EXOTEC\Questionaire\Domain\Model\ResultNote $noteToRemove The ResultNote to be removed
     * @return void
     */
    public function removeNote(\EXOTEC\Questionaire\Domain\Model\ResultNote $noteToRemove)
    {
        $this->notes->detach($noteToRemove);
    }

    /**
     * Returns the notes
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\ResultNote> $notes
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Sets the notes
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\Questionaire\Domain\Model\ResultNote> $notes
     * @return void
     */
    public function setNotes(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return int
     */
    public function getLanguageUid()
    {
        return $this->_languageUid;
    }

    /**
     * @param int $languageUid
     */
    public function setLanguageUid($languageUid)
    {
        $this->_languageUid = $languageUid;
    }


}
