<?php
namespace EXOTEC\Questionaire\Domain\Model;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

/**
 * Matrixquestion
 */
class Matrixquestion extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * checked - just a pseudo prop for the results show action
     *
     * @var string
     */
    protected $checked = '';

    /**
     * @return string
     */
    public function getChecked ()
    {
        return $this->checked;
    }

    /**
     * @param string $checked
     */
    public function setChecked ($checked)
    {
        $this->checked = $checked;
    }


    /**
     * resultAnswers - just a pseudo prop for the backend show action
     *
     * @var array
     */
    protected $resultAnswers = '';

    /**
     * @return array
     */
    public function getResultAnswers ()
    {
        return $this->resultAnswers;
    }

    /**
     * @param array $resultAnswers
     */
    public function setResultAnswers ($resultAnswers)
    {
        $this->resultAnswers = $resultAnswers;
    }

    /**
     * minAnswers
     *
     * @var int
     */
    protected $minAnswers = 0;


    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * @return string
     */
    public function getTitle ()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle ($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getMinAnswers ()
    {
        return $this->minAnswers;
    }

    /**
     * @param int $minAnswers
     */
    public function setMinAnswers ($minAnswers)
    {
        $this->minAnswers = $minAnswers;
    }




}
