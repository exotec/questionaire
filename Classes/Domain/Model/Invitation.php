<?php
namespace EXOTEC\Questionaire\Domain\Model;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/


/**
 * Invitation
 */
class Invitation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{


    /**
     * feUser
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $feUser = 0;

    /**
     * feUser
     *
     * @var \EXOTEC\Questionaire\Domain\Model\Survey
     */
    protected $survey = 0;

    /**
     * result
     *
     * @var int

     */
    protected $result = '';

    /**
     * surveyStarted
     *
     * @var bool

     */
    protected $surveyStarted = null;

    /**
     * surveyStartedDate
     *
     * @var int

     */
    protected $surveyStartedDate = 0;

    /**
     * surveyCompleted
     *
     * @var bool

     */
    protected $surveyCompleted = null;

    /**
     * surveyCompletedDate
     *
     * @var int

     */
    protected $surveyCompletedDate = 0;

    /** @var int */
    protected $crdate;


    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    public function getFeUser()
    {
        return $this->feUser;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $feUser
     */
    public function setFeUser($feUser)
    {
        $this->feUser = $feUser;
    }

    /**
     * @return Survey
     */
    public function getSurvey()
    {
        return $this->survey;
    }

    /**
     * @param Survey $survey
     */
    public function setSurvey($survey)
    {
        $this->survey = $survey;
    }

    /**
     * @return int
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param int $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }



    /**
     * @return bool
     */
    public function isSurveyStarted()
    {
        return $this->surveyStarted;
    }

    /**
     * @param bool $surveyStarted
     */
    public function setSurveyStarted($surveyStarted)
    {
        $this->surveyStarted = $surveyStarted;
    }

    /**
     * @return int
     */
    public function getSurveyStartedDate()
    {
        return $this->surveyStartedDate;
    }

    /**
     * @param int $surveyStartedDate
     */
    public function setSurveyStartedDate($surveyStartedDate)
    {
        $this->surveyStartedDate = $surveyStartedDate;
    }

    /**
     * @return bool
     */
    public function isSurveyCompleted()
    {
        return $this->surveyCompleted;
    }

    /**
     * @param bool $surveyCompleted
     */
    public function setSurveyCompleted($surveyCompleted)
    {
        $this->surveyCompleted = $surveyCompleted;
    }

    /**
     * @return int
     */
    public function getSurveyCompletedDate()
    {
        return $this->surveyCompletedDate;
    }

    /**
     * @param int $surveyCompletedDate
     */
    public function setSurveyCompletedDate($surveyCompletedDate)
    {
        $this->surveyCompletedDate = $surveyCompletedDate;
    }

    /**
     * _languageUid
     *
     * @var int

     */
    protected $_languageUid = 0;


    /**
     * Returns the crdate
     *
     * @return int
     */
    public function getCrdate() {
        return $this->crdate;
    }

    /**
     * @return int
     */
    public function getLanguageUid()
    {
        return $this->_languageUid;
    }

    /**
     * @param int $languageUid
     */
    public function setLanguageUid($languageUid)
    {
        $this->_languageUid = $languageUid;
    }

//    /**
//     * sysLanguageUid
//     *
//     * @var int
//
//     */
//    protected $sysLanguageUid = 0;
//
//    /**
//     * @return int
//     */
//    public function getSysLanguageUid()
//    {
//        return $this->sysLanguageUid;
//    }
//
//    /**
//     * @param int $sysLanguageUid
//     */
//    public function setSysLanguageUid($sysLanguageUid)
//    {
//        $this->sysLanguageUid = $sysLanguageUid;
//    }






}
