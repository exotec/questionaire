<?php
namespace EXOTEC\Questionaire\Domain\Model;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

/**
 * FrontendUser
 */
class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{
    /**
     * gender
     *
     * @var int

     */
    protected $gender = 0;

    /**
     * staticInfoCountry
     *
     * @var string

     */
    protected $staticInfoCountry = '';


    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getStaticInfoCountry()
    {
        return $this->staticInfoCountry;
    }

    /**
     * @param string $staticInfoCountry
     */
    public function setStaticInfoCountry($staticInfoCountry)
    {
        $this->staticInfoCountry = $staticInfoCountry;
    }




}
