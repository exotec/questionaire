<?php
namespace EXOTEC\Questionaire\Domain\Model;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

/**
 * ResultAnswer
 */
class ResultAnswer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * $languageUid
     *
     * @var int

     */
    protected $languageUid = 0;

    /**
     * @return int
     */
    public function getLanguageUid()
    {
        return $this->_languageUid;
    }

    /**
     * @param int $languageUid
     */
    public function setLanguageUid($languageUid)
    {
        $this->_languageUid = $languageUid;
    }

    /**
     * question
     *
     * @var int
     */
    protected $question = 0;

    /**
     * answer
     *
     * @var string
     */
    protected $answer = '';

    /**
     * type
     *
     * @var string
     */
    protected $type = '';

    /**
     * Returns the question
     *
     * @return int $question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Sets the question
     *
     * @param int $question
     * @return void
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return string
     */
    public function getAnswer ()
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     */
    public function setAnswer ($answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return string
     */
    public function getType ()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType ($type)
    {
        $this->type = $type;
    }


}
