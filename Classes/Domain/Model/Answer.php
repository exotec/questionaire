<?php
namespace EXOTEC\Questionaire\Domain\Model;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Answer
 */
class Answer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    
    /**
     * checked - just a pseudo prop for the results show action
     *
     * @var string
     */
    protected $checked = '';

    /**
     * $percents - just a pseudo prop for the backend show action
     *
     * @var integer
     */
    protected $percents = 0;

    /**
     * $total - just a pseudo prop for the backend show action
     *
     * @var integer
     */
    protected $total = 0;

    /**
     * title
     *
     * @var string
     */
    protected $title = '';


    /**
     * textareaPlaceholder
     *
     * @var string
     */
    protected $textareaPlaceholder = '';

    /**
     * @return string
     */
    public function getChecked ()
    {
        return $this->checked;
    }

    /**
     * @param string $checked
     */
    public function setChecked ($checked)
    {
        $this->checked = $checked;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getPercents()
    {
        return $this->percents;
    }

    /**
     * @param int $percents
     */
    public function setPercents($percents)
    {
        $this->percents = $percents;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getTextareaPlaceholder()
    {
        return $this->textareaPlaceholder;
    }

    /**
     * @param string $textareaPlaceholder
     */
    public function setTextareaPlaceholder($textareaPlaceholder)
    {
        $this->textareaPlaceholder = $textareaPlaceholder;
    }


}
