<?php

namespace EXOTEC\Questionaire\Controller;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

use Doctrine\Common\Util\Debug;
use EXOTEC\Questionaire\Domain\Model\Answer;
use EXOTEC\Questionaire\Domain\Model\FrontendUser;
use EXOTEC\Questionaire\Domain\Model\Invitation;
use EXOTEC\Questionaire\Domain\Model\Question;
use EXOTEC\Questionaire\Domain\Model\Result;
use EXOTEC\Questionaire\Domain\Model\ResultAnswer;
use EXOTEC\Questionaire\Domain\Model\ResultNote;
use EXOTEC\Questionaire\Domain\Model\Survey;
use EXOTEC\Questionaire\Domain\Repository\FrontendUserRepository;
use EXOTEC\Questionaire\Domain\Repository\InvitationRepository;
use EXOTEC\Questionaire\Domain\Repository\MatrixquestionRepository;
use EXOTEC\Questionaire\Domain\Repository\QuestionRepository;
use EXOTEC\Questionaire\Domain\Repository\ResultRepository;
use EXOTEC\Questionaire\Domain\Repository\SurveyRepository;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * ResultController
 */
class ResultController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController implements \Countable
{
    private $count = 0;

    public function count()
    {
        return ++$this->count;
    }

    /**
     * resultRepository
     *
     * @var ResultRepository
     */
    protected $resultRepository = null;

    /**
     * @param ResultRepository $resultRepository
     */
    public function injectResultRepository(ResultRepository $resultRepository)
    {
        $this->resultRepository = $resultRepository;
    }

    /**
     * questionRepository
     *
     * @var QuestionRepository
     */
    protected $questionRepository = null;

    /**
     * @param QuestionRepository $questionRepository
     */
    public function injectQuestionRepository(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * matrixquestionRepository
     *
     * @var MatrixquestionRepository
     */
    protected $matrixquestionRepository = null;

    /**
     * @param MatrixquestionRepository $matrixquestionRepository
     */
    public function injectMatrixquestionRepository(MatrixquestionRepository $matrixquestionRepository)
    {
        $this->matrixquestionRepository = $matrixquestionRepository;
    }

    /**
     * surveyRepository
     *
     * @var SurveyRepository
     */
    protected $surveyRepository = null;

    /**
     * @param SurveyRepository $surveyRepository
     */
    public function injectSurveyRepository(SurveyRepository $surveyRepository)
    {
        $this->surveyRepository = $surveyRepository;
    }

    /**
     * frontendUserRepository
     *
     * @var FrontendUserRepository
     */
    protected $frontendUserRepository = null;

    /**
     * @param FrontendUserRepository $frontendUserRepository
     */
    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }


    /**
     * invitationRepository
     *
     * @var InvitationRepository
     */
    protected $invitationRepository = null;

    /**
     * @param InvitationRepository $invitationRepository
     */
    public function injectInvitationRepository(InvitationRepository $invitationRepository)
    {
        $this->invitationRepository = $invitationRepository;
    }


    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $args = $this->request->getArguments();
        $email = $args['email'];
        $results = $this->resultRepository->findByEmail($email);

        // if the first char wildcard *
        if ($email[0] == '*') {
            $atDomain = ltrim($email, '*');
            $results = $this->resultRepository->findAllByDomainOfEmail($atDomain);
        }

        $wildCardDomain = GeneralUtility::trimExplode('@', $email);
        $this->view->assign('wildCardDomain', $wildCardDomain[1]);
        $this->view->assign('resultss', $results);
        $this->view->assign('email', $email);

        $hideAllLink = 0;
        if ($args['hideAllLink']) {
            $hideAllLink = 1;
        }
        $this->view->assign('hideAllLink', $hideAllLink);

    }


    /**
     * @param Result $result
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("result")
     */
    public function showAction(\EXOTEC\Questionaire\Domain\Model\Result $result)
    {
        $surveyUid = $result->getSurvey();

        /** @var Survey $survey */
        $survey = $this->surveyRepository->findByUid($surveyUid);
        $questions = $survey->getQuestions();

        // build an array with resultAnswer uid's which we can compare in the foreach's
        /** @var ResultAnswer $resultAnswer */
        foreach ($result->getAnswers() as $resultAnswer) {
            $resultAnswers[$resultAnswer->getUid()] = $resultAnswer->getAnswer();

            if ($resultAnswer->getType() == 'Matrix') {
                $matrixAnswers[$resultAnswer->getQuestion()] = $resultAnswer->getAnswer();
            }
        }

        /** @var Question $answer */
        foreach ($questions as $question) {
            $type = $question->getType();
            foreach ($question->getAnswers() as $answer) {
                if ($type != 'Matrix') {
                    $answers[] = $answer;
                }
            }
            foreach ($question->getMatrixquestions() as $matrixquestion) {
                $matrixquestions[$matrixquestion->getUid()] = $matrixAnswers[$matrixquestion->getUid()];
            }
        }

        /** @var Answer $answer */
        foreach ($answers as $key => $answer) {
            $uid = $answer->getUid();
            if (in_array($uid, $resultAnswers)) {
                $answer->setChecked(1);
            }

        }

        // set grades
        $grades = GeneralUtility::trimExplode(',', $this->settings['grades']);
        foreach ($grades as $key => $value) {
            $gradesArr[$key]['uid'] = $value;
            $gradesArr[$key]['points'] = $value;
            $gradesArr[$key]['title'] = LocalizationUtility::translate('label.grade.' . $value, 'Questionaire');;
            if (in_array('grade-' . $value, $resultAnswers)) {
                $gradesArr[$key]['checked'] = 1;
            }
        }

        // build an array with the resultNotes which we can put in the textareas
        /** @var ResultNote $note */
        foreach ($result->getNotes() as $note) {
            if ($note->getType() == 'Matrix') {
                $matrixquestion = $this->matrixquestionRepository->findByUid($note->getQuestion());
                $matrixnotes[$matrixquestion->getUid()] = $note->getNote();
            } else {
                $question = $this->questionRepository->findByUid($note->getQuestion());
                $notes[$question->getUid()] = $note->getNote();
            }
        }

        $surveyController = new SurveyController();
        /** @var Question $question */
        foreach ($survey->getQuestions() as $index => $question) {
            if ($question->getType() == 'Radios' && $question->getHtmlElement() == 'Select') {
                /** @var ResultAnswer $answer */
                foreach ($result->getAnswers() as $answer) {
                    if ($answer->getType() == 'Radios' && $answer->getQuestion() == $question->getUid()) {
                        $options = $surveyController->getSelectOptions($question->getAnswers(), $answer->getAnswer());
                    }
                }
                $this->view->assign('selectOptions', $options);
            }
        }

        $this->view->assign('notes', $notes);
        $this->view->assign('matrixnotes', $matrixnotes);
        $this->view->assign('matrixanswers', $matrixquestions);
        $this->view->assign('grades', $gradesArr);
        $this->view->assign('survey', $survey);
        $this->view->assign('disabled', 1);

        /** @var FrontendUser $feUser */
        $feUser = $this->frontendUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);

        /** @var Invitation $invitation */
        $invitation = $this->invitationRepository->findByFeUser($feUser);

        if ($this->settings['doNotUseAuthKey'] == 1) {
            $lastInvitationKey = $invitation->count() - 1;
            $lastInvitation = new \stdClass();
            $lastInvitation = $invitation;
            unset($invitation);
            $invitation = $lastInvitation[$lastInvitationKey];
        } else {
            $invitation = $invitation->current();
        }

        // do nothing more if the user reloads the result page
        if ($invitation && $invitation->isSurveyCompleted()) {
            return;
        }

        if ($invitation) {

            // set invitation completed values
            $invitation->setSurveyCompleted(1);
            $invitation->setSurveyCompletedDate(time());
            $this->invitationRepository->update($invitation);
            $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
            $persistenceManager->persistAll();

            // sending mails
            // the user confirmation mail
            $templateName = 'ConfirmationEmail';

            $temVars = array(
                'first_name' => $feUser->getFirstName(),
                'last_name' => $feUser->getLastName()
            );
            $html = $this->getTemplateHtml("Survey/$templateName.html", $temVars);
            $subject = LocalizationUtility::translate('label.confirmationMail.subject', 'Questionaire');
            $this->sendMail($feUser->getEmail(), $this->settings['adminEmailFrom'], $this->settings['adminEmailFromName'], $subject, $html, 'text/plain');

            $text = 'Username: ' . $feUser->getUsername() . '<br />';
            $text .= 'Company: ' . $feUser->getCompany() . '<br />';
            $text .= 'Email: ' . $feUser->getEmail() . '<hr />';
        }

        // the admin mail
        $temVars = array(
            'survey' => $survey,
            'notes' => $notes,
            'matrixnotes' => $matrixnotes,
            'matrixanswers' => $matrixquestions,
            'grades' => $gradesArr,
            'disabled' => 1,
            'selectOptions' => $options
        );
        $html = $text . $this->getTemplateHtml('Result/Show.html', $temVars);

        $this->sendMail($this->settings['adminEmail'], $this->settings['adminEmailFrom'], $this->settings['adminEmailFromName'], $this->settings['adminEmailSubject'], $html, 'text/html');

    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {

    }


    /**
     * action create
     *
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function createAction()
    {
        $args = $this->request->getArguments();
        $newResult = new Result();
        $newResult->setSurvey((int)$args['values']['survey']);

        $feUser = $this->frontendUserRepository->findByUid($args['values']['feUser']);
        $newResult->setFeUser($feUser);

        $languageAspect = GeneralUtility::makeInstance(Context::class)->getAspect('language');
        $sys_language_uid = $languageAspect->getId();
        $newResult->setLanguageUid($sys_language_uid);

        foreach ($args['values'] as $questionUid => $values) {
            // set result answers
            if ($questionUid != 'notes' && $questionUid != 'survey' && $questionUid != 'feUser' && $questionUid != 'invitation_uid') {

                if (is_array($values)) {
                    foreach ($values as $index => $value) {
                        if ($questionUid != 'matrix') {
                            // checkboxes
                            $answer = new ResultAnswer();
                            $answer->setType('Checkboxes');
                            $answer->setQuestion($questionUid);
                            $answer->setAnswer($value);
                            $answer->setPid($this->settings['storagePid']);
                            $answer->setLanguageUid($sys_language_uid);
                            $newResult->addAnswer($answer);
                        }
                    }
                } else {
                    // radios
                    $answer = new ResultAnswer();
                    $answer->setType('Radios');
                    $answer->setQuestion($questionUid);
                    $answer->setAnswer($values);
                    $answer->setPid($this->settings['storagePid']);
                    $answer->setLanguageUid($sys_language_uid);
                    $newResult->addAnswer($answer);
                }
            } else {
                // notes array
                $notesArr['notes'] = $values;
            }
        }

        foreach ($args['values']['matrix'] as $questionUid => $answerUid) {
            $answer = new ResultAnswer();
            $answer->setType('Matrix');
            $answer->setQuestion($questionUid);
            $answer->setAnswer($answerUid);
            $answer->setPid($this->settings['storagePid']);
            $answer->setLanguageUid($sys_language_uid);
            $newResult->addAnswer($answer);
        }


        // set the notes
        foreach ($notesArr['notes'] as $questionUid => $value) {

            if ($questionUid == 'matrix') {
                foreach ($value as $uid => $item) {
                    $note = new ResultNote();
                    $note->setQuestion($uid);
                    $note->setNote($item);
                    $note->setType('Matrix');
                    $note->setPid($this->settings['storagePid']);
                    $note->setLanguageUid($sys_language_uid);
                    $newResult->addNote($note);
                }
            } else {
                $note = new ResultNote();
                $note->setQuestion($questionUid);
                $note->setNote($value);
                $note->setPid($this->settings['storagePid']);
                $note->setLanguageUid($sys_language_uid);
                $newResult->addNote($note);
            }

        }
        $newResult->setPid($this->settings['storagePid']);

        $this->addFlashMessage(LocalizationUtility::translate('label.survey.completed.message', 'Questionaire'), LocalizationUtility::translate('label.survey.completed.header', 'Questionaire'), \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
        $this->resultRepository->add($newResult);

        $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
        $persistenceManager->persistAll();

        if ($this->settings['doNotUseAuthKey'] == 1) {
            $invitation = $this->invitationRepository->findBySurvey($args['values']['survey']);
            $lastInvitationKey = $invitation->count() - 1;
            $lastInvitation = new \stdClass();
            $lastInvitation = $invitation;
            unset($invitation);
            $invitation = $lastInvitation[$lastInvitationKey];
        } else {
            /** @var Invitation $invitation */
            $invitation = $this->invitationRepository->findByUid($args['values']['invitation_uid']);
        }

        /** @var Result $result */
        $result = $this->resultRepository->findByUid($newResult->getUid());
        // update result
        $result->setInvitation($invitation->getUid());
        $this->resultRepository->update($result);

        // update invitation
        $invitation->setResult($result->getUid());
        $this->invitationRepository->update($invitation);

        $this->redirect('show', 'Result', 'Questionaire', array('result' => $result), $this->settings['detailPid']);
    }

    /**
     * gets the HTML of Result Show action for sending email
     *
     * @param $controllerName
     * @param $templateName
     * @param $variables
     * @return mixed
     */
    public function getTemplateHtml($template, $variables)
    {

        /** @var \TYPO3\CMS\Fluid\View\StandaloneView $emailView */
        $tempView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');

        $extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        $tempView->setTemplateRootPaths($extbaseFrameworkConfiguration['view']['templateRootPaths']);
        $tempView->setLayoutRootPaths($extbaseFrameworkConfiguration['view']['layoutRootPaths']);
        $tempView->setPartialRootPaths($extbaseFrameworkConfiguration['view']['partialRootPaths']);

        $tempView->setTemplate($template);

        $tempView->assignMultiple($variables);
        $tempHtml = $tempView->render();

        return $tempHtml;
    }


    /**
     * @param $toEmail
     * @param $fromEmail
     * @param $fromName
     * @param $subject
     * @param $text
     * @param $contentType
     * @return bool
     */
    public function sendMail($toEmail, $fromEmail, $fromName, $subject, $text, $contentType)
    {
        $message = GeneralUtility::makeInstance(MailMessage::class);
        $message->setSubject($subject)
            ->setFrom(array($fromEmail => $fromName))
            ->setTo(array($toEmail))
            ->html($text, $contentType);
        if ($message->send()) {
            return true;
        }
        return false;
    }


}
