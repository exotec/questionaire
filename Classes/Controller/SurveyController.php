<?php
namespace EXOTEC\Questionaire\Controller;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/

use EXOTEC\Questionaire\Domain\Model\Invitation;
use EXOTEC\Questionaire\Domain\Model\Question;
use EXOTEC\Questionaire\Domain\Repository\InvitationRepository;
use EXOTEC\Questionaire\Domain\Repository\SurveyRepository;
use EXOTEC\Questionaire\Utility\Crypt;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * SurveyController
 */
class SurveyController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController implements \Countable
{

//    private PersistenceManager $persistenceManager;
//
//    public function __construct(PersistenceManager $persistenceManager)
//    {
//        $this->persistenceManager = $persistenceManager;
//    }

    /**
     * surveyRepository
     *
     * @var SurveyRepository
     */
    protected $surveyRepository = null;


    /**
     * @param SurveyRepository $surveyRepository
     */
    public function injectSurveyRepository(SurveyRepository $surveyRepository)
    {
        $this->surveyRepository = $surveyRepository;
    }

    /**
     * invitationRepository
     *
     * @var InvitationRepository
     */
    protected $invitationRepository = null;

    /**
     * @param InvitationRepository $invitationRepository
     */
    public function injectInvitationRepository(InvitationRepository $invitationRepository)
    {
        $this->invitationRepository = $invitationRepository;
    }

    private $count = 0;

    public function count() {
        return ++$this->count;
    }


    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $surveys = $this->surveyRepository->findAll();
        $this->view->assign('surveys', $surveys);
        $this->view->assign('doNotUseAuthKey', $this->settings['doNotUseAuthKey']);
    }


    /**
     * @param \EXOTEC\Questionaire\Domain\Model\Survey $survey
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function showAction(\EXOTEC\Questionaire\Domain\Model\Survey $survey)
    {
        /** @var ObjectManager $this->objectManager */
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->persistenceManager = $this->objectManager->get(PersistenceManager::class);

        if( $this->settings['doNotUseAuthKey'] == 0 ) {
            /** @var Crypt $crypt */
            $crypt = $this->objectManager->get(Crypt::class);

            // build array from URL params
            $key = $this->request->getArgument('key');
            $decryptedParams = $crypt->decryptGetParams($key);
            $url = '/?0'.$decryptedParams;
            $params = [];
            parse_str(parse_url($url, PHP_URL_QUERY), $params);

            /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
            $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
            $querySettings->setRespectSysLanguage(FALSE);
            $querySettings->setRespectStoragePage(FALSE);
            $this->invitationRepository->setDefaultQuerySettings($querySettings);

            $invited2Survey = $params['invited2Survey'];

            // no invitation param in link found
            if( !intval($params['invitation_uid']) || $invited2Survey != $survey->getUid() ) {
                $this->addFlashMessage(
                    LocalizationUtility::translate('label.error.notAllowedForThisSurvey','Questionaire'),
                    LocalizationUtility::translate('label.error.header','Questionaire'), \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
                $this->view->assign('noValidUser', true);
                return;
            }
            /** @var Invitation $invitation */
            $invitation = $this->invitationRepository->findByUid($params['invitation_uid']);

            // error messages
            // invitation exits, but is not valid with link parameter user
            if($GLOBALS['TSFE']->fe_user->user['uid'] != $invitation->getFeUser()->getUid()) {
                $this->addFlashMessage(
                    LocalizationUtility::translate('label.error.wrongInvitationLink','Questionaire'),
                    LocalizationUtility::translate('label.error.header','Questionaire'), \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
                $this->view->assign('noValidUser', true);

            } elseif ( $invitation->isSurveyCompleted() == true ) {
                // user has allways completed this survey
                $this->addFlashMessage(
                    LocalizationUtility::translate('label.error.hasParticipated', 'Questionaire'),
                    LocalizationUtility::translate('label.error.header', 'Questionaire'), \TYPO3\CMS\Core\Messaging\AbstractMessage::INFO);
                $this->view->assign('noValidUser', true);
            }
//            } else {
                // update invitation and set start values
                $invitation->setSurveyStarted(1);
                $invitation->setSurveyStartedDate(time());
                $this->invitationRepository->update($invitation);
                $this->persistenceManager->persistAll();
//            }
        } else {


            /** @var Invitation $invitation */
            $invitation = new Invitation();
            $invitation->setFeUser($GLOBALS['TSFE']->fe_user->user['uid']);
            $invitation->setPid($this->settings['storagePid']);
            $invitation->setSurveyStarted(1);
            $invitation->setSurveyStartedDate(time());
            $invitation->setResult(0);
            $invitation->setSurvey($survey->getUid());

            // create a fake invitation
            $this->invitationRepository->add($invitation);
            $this->persistenceManager->persistAll();
        }

        $this->view->assign('doNotUseAuthKey', $this->settings['doNotUseAuthKey']);
        $this->view->assign('feUser', $GLOBALS['TSFE']->fe_user->user['uid']);
        $this->view->assign('survey', $survey);
        $this->view->assign('invitation_uid', $params['invitation_uid']);

        /** @var Question $question */
        foreach ($survey->getQuestions() as $index => $question) {
            if($question->getType() == 'Radios' && $question->getHtmlElement() == 'Select') {
                $options = $this->getSelectOptions($question->getAnswers());
                $this->view->assign('selectOptions', $options);
            }
        }

        // set grades
        $grades = GeneralUtility::trimExplode(',', $this->settings['grades']);
        foreach ($grades as $key => $value) {
            $gradesArr[$key]['title'] = LocalizationUtility::translate('label.grade.'.$value,'Questionaire');;
            $gradesArr[$key]['uid'] = 'grade-'.$value;
            $gradesArr[$key]['points'] = $value;
        }
        $this->view->assign('grades', $gradesArr);

        // build inline JS
        $JS = $this->buildInlineJS($survey, $JS);

        $this->view->assign('inlineJS', $JS);
    }

    /**
     * prepare categories for select box
     *
     * @return array
     */
    public function getSelectOptions($items, $selected="") {
        $options = array();
        foreach ($items as $item) {
            $option = new \stdClass();
            $option->value = $item->getUid();
            $option->label = $item->getTitle();

            if( intval($selected) == $item->getUid() ) {
                $option->selected = true;
            }

            $options[] = $option;
        }
        return $options;
    }

    /**
     * @param \EXOTEC\Questionaire\Domain\Model\Survey $survey
     * @param $JS
     * @return string
     */
    protected function buildInlineJS (\EXOTEC\Questionaire\Domain\Model\Survey $survey, $JS)
    {
        $i = 0;
        /** @var Question $question */
        foreach ($survey->getQuestions() as $question) {
            $a = $i++;
            $minAnswers = 1;
            if(  $question->getType() == 'Checkboxes' ) {
                $minAnswers = $question->getMinAnswers();
            }
            $maxAnswers = $question->getMaxAnswers();
            $suffix = '';
            if($minAnswers > 1) {
                $suffix = 'en';
            }
            if ($question->getType() != 'Matrix') {
                $JS .= $this->buildJsVars($question, $a, $minAnswers, $suffix, $maxAnswers, $question->getType(), $question->getUid());
            } else {
                $i = 0;
                foreach ($question->getMatrixquestions() as $matrixquestion) {
                    $b = $i++;
                    $minAnswers = 1;
                    $suffix = '';
                    if($minAnswers > 1) {
                        // means Antwort"en"
                        $suffix = 'en';
                    }
                    $JS .= $this->buildJsVars($matrixquestion, $b, $minAnswers, $suffix, $maxAnswers, $question->getType(), $matrixquestion->getUid());
                }
            }
        }

        return $JS;
    }


    /**
     * @param $question
     * @param $i
     * @param $minAnswers
     * @param $suffix
     * @param $maxAnswers
     * @param $type
     * @param $questionUid
     * @return string
     */
    protected function buildJsVars($question, $i, $minAnswers, $suffix, $maxAnswers, $type, $questionUid)
    {
        $JS = '
            var ' . $type . '_error_label' . $i . ' = "' . LocalizationUtility::translate('label.error', 'Questionaire', array($minAnswers, $suffix)) . '";
            var ' . $type . '_error_label_max' . $i . ' = "' . LocalizationUtility::translate('label.error.max', 'Questionaire', array($maxAnswers)) . '";
            var className = ".' . $type . '-' . $questionUid . '";
            if( !validateMultipleCheckBoxes(className, ' . $minAnswers . ', ' . $maxAnswers . ',"' . $type . '_error_' . $questionUid . '", ' . $type . '_error_label' . $i . ',' . $type . '_error_label_max' . $i . ') ){
                error = 1;
            } 
        ';
        return $JS;
    }
}
