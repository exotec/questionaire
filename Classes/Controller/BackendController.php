<?php
namespace EXOTEC\Questionaire\Controller;

/***
 *
 * This file is part of the "Questionaire" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/


use EXOTEC\Questionaire\Domain\Model\Answer;
use EXOTEC\Questionaire\Domain\Model\Question;
use EXOTEC\Questionaire\Domain\Model\Result;
use EXOTEC\Questionaire\Domain\Model\ResultAnswer;
use EXOTEC\Questionaire\Domain\Model\ResultNote;
use EXOTEC\Questionaire\Domain\Model\Survey;
use EXOTEC\Questionaire\Domain\Repository\InvitationRepository;
use EXOTEC\Questionaire\Domain\Repository\MatrixquestionRepository;
use EXOTEC\Questionaire\Domain\Repository\QuestionRepository;
use EXOTEC\Questionaire\Domain\Repository\ResultAnswerRepository;
use EXOTEC\Questionaire\Domain\Repository\ResultRepository;
use EXOTEC\Questionaire\Domain\Repository\SurveyRepository;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

require __DIR__ . '/../Utility/vendor/phpoffice/phpspreadsheet/src/Bootstrap.php';

/**
 * BackendController
 */
class BackendController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController implements \Countable
{

    private $count = 0;

    public function count() {
        return ++$this->count;
    }

    /**
     * surveyRepository
     *
     * @var SurveyRepository
     */
    protected $surveyRepository = null;

    /**
     * @param SurveyRepository $surveyRepository
     */
    public function injectSurveyRepository(SurveyRepository $surveyRepository)
    {
        $this->surveyRepository = $surveyRepository;
    }

    /**
     * resultRepository
     *
     * @var ResultRepository
     */
    protected $resultRepository = null;

    /**
     * @param ResultRepository $resultRepository
     */
    public function injectResultRepository(ResultRepository $resultRepository)
    {
        $this->resultRepository = $resultRepository;
    }

    /**
     * resultAnswerRepository
     *
     * @var ResultAnswerRepository
     */
    protected $resultAnswerRepository = null;

    /**
     * @param ResultAnswerRepository $resultAnswerRepository
     */
    public function injectResultAnswerRepository(ResultAnswerRepository $resultAnswerRepository)
    {
        $this->resultAnswerRepository = $resultAnswerRepository;
    }

    /**
     * matrixquestionRepository
     *
     * @var MatrixquestionRepository
     */
    protected $matrixquestionRepository = null;

    /**
     * @param MatrixquestionRepository $matrixquestionRepository
     */
    public function injectMatrixquestionRepository(MatrixquestionRepository $matrixquestionRepository)
    {
        $this->matrixquestionRepository = $matrixquestionRepository;
    }

    /**
     * questionRepository
     *
     * @var QuestionRepository
     */
    protected $questionRepository = null;

    /**
     * @param QuestionRepository $questionRepository
     */
    public function injectQuestionRepository(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * invitationRepository
     *
     * @var InvitationRepository
     */
    protected $invitationRepository = null;

    /**
     * @param InvitationRepository $invitationRepository
     */
    public function injectInvitationRepository(InvitationRepository $invitationRepository)
    {
        $this->invitationRepository = $invitationRepository;
    }


    /**
     * action index
     * @throws \Exception
     */
    public function indexAction()
    {
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectSysLanguage(FALSE);
        $querySettings->setRespectStoragePage(FALSE);
        $this->surveyRepository->setDefaultQuerySettings($querySettings);

        $surveys = $this->surveyRepository->findAll();
        $this->view->assign('surveys', $surveys);

        $this->invitationRepository->setDefaultQuerySettings($querySettings);
        $invitations = $this->invitationRepository->findAll();
        $this->view->assign('invitations', $invitations);

        // Counter
        $results = $this->resultRepository->findAll();
        $x =1 ;
        /** @var Result $result */
        foreach ($results as $result) {
            $i = $x++;
            $counter[$result->getSurvey()] = $i;
        }

        if( $this->count($counter) < $this->count($surveys)) {
            foreach ($surveys as $survey) {
                if(!$counter[$survey->getUid()]) {
                    $counter[$survey->getUid()] = 'keine';
                }
            }
        }
        $this->view->assign('counter', $counter);

    }

    /**
     * overview list
     * @throws \Exception
     */
    public function overviewAction()
    {

        $arguments = $this->request->getArguments();
        $this->view->assign('arguments', $arguments);

        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectSysLanguage(FALSE);
        $querySettings->setRespectStoragePage(FALSE);
        $this->surveyRepository->setDefaultQuerySettings($querySettings);

        $survey = $this->surveyRepository->findByUid($arguments['survey']);
        $this->view->assign('survey', $survey);

        $this->invitationRepository->setDefaultQuerySettings($querySettings);
        $invitations = $this->invitationRepository->findBySurvey($arguments['survey']);
        $this->view->assign('invitations', $invitations);

        $closedInvitations = $this->invitationRepository->findClosedInvitationsBySurvey($arguments['survey']);
        $this->view->assign('closedInvitations', $closedInvitations);

        $openInvitations = $this->invitationRepository->findOpenInvitationsBySurvey($arguments['survey']);
        $this->view->assign('openInvitations', $openInvitations);

        $canceledInvitations = $this->invitationRepository->findCanceledInvitationsBySurvey($arguments['survey']);
        $this->view->assign('canceledInvitations', $canceledInvitations);

        $results = $this->resultRepository->findAll();
        $x =1 ;
        /** @var Result $result */
        foreach ($results as $result) {
            $i = $x++;
            $counter[$result->getSurvey()] = $i;
        }

        $this->view->assign('counter', $counter);


        // The Chart
        if( count($invitations) == 0 ) {
            $this->view->assign('JSdates', '[]');
            $this->view->assign('JSclosedInvitations', '[]');
            $this->view->assign('JSopenInvitations', '[]');
            $this->view->assign('JSstartedInvitations', '[]');
            $this->view->assign('allInvitations', 0);
            $this->view->assign('JSstartDate', 0);
            $this->view->assign('JScountDays', 0);
            return;
        }

        // days since start
        $period = new \DatePeriod(
            new \DateTime(date('Y-m-d', $invitations->getFirst()->getCrDate())),
            new \DateInterval('P1D'),
            new \DateTime('now')
        );

        foreach ($period as $i => $date) {
            $countItems = $i;
        }

        /** @var Date $date */
        $x = 0;
        foreach ($period as $date) {
            // days iteration
            $i = $x++;

            // {JScountDays} VAR
            $daysAsNumbers[$i] = $date->format('d.m.Y');

            // get actual next day, to check if a invitation is on same day
            $datetime = new \DateTime($date->format('Y-m-d'));
            $nextDay = $datetime->modify('+1 day');

            // {JSclosedInvitations} VAR
            $counterClosed += $this->invitationRepository->findClosedByDateInRange($date->getTimestamp(),$nextDay)->count();
            $JSclosedInvitations[$i] = $counterClosed;

            // {JSstartedInvitations} VAR
            $counterStarted += $this->invitationRepository->findStartedByDateInRange($date->getTimestamp(),$nextDay)->count();
            $JSstartedInvitations[$i] = $counterStarted;

            // for open invitations
            if ($i == $countItems) {
                $pointSize[$i] = 12;
                $borderWidth[$i] = 2;
                $JSopenInvitations[$i] = $openInvitations->count();
            } else {
                $pointSize[$i] = 0;
                $borderWidth[$i] = 0;
                $JSopenInvitations[$i] = 0;
            }

        }

        $this->view->assign('JSdates', json_encode($daysAsNumbers));
        $this->view->assign('JSclosedInvitations', json_encode($JSclosedInvitations));
        $this->view->assign('JSopenInvitations', json_encode($JSopenInvitations));
        $this->view->assign('JSstartedInvitations', json_encode($JSstartedInvitations));
        $this->view->assign('JScountDays', count($daysAsNumbers));
        $this->view->assign('JSstartDate', date('d.m.Y',$invitations->getFirst()->getCrDate()));
        $this->view->assign('JSpointSizeOpen', json_encode($pointSize));
        $this->view->assign('JScountOpen', $openInvitations->count());


    }


    /**
     * @param \EXOTEC\Questionaire\Domain\Model\Survey $survey
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function csvAction(\EXOTEC\Questionaire\Domain\Model\Survey $survey) {

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('GÖPEL electronic')
            ->setLastModifiedBy('GÖPEL electronic')
            ->setTitle( $survey->getTitle() )
            ->setSubject($survey->getTitle() )
            ->setDescription( $survey->getDescription() )
            ->setKeywords('')
            ->setCategory('Umfragen');

        $styleQuestion = [
            'font' => [
                'bold' => true,
                'size' => 14,
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => [
                    'rgb' => 'CCCCCC',
                ],
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];

        $styleMatrixquestion = [
            'font' => [
                'bold' => true,
                'size' => 12,
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => [
                    'rgb' => 'EEEEEE',
                ],
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];

        $gradesData = [
            ['sehr gut', 'grade-1'],
            ['gut', 'grade-2'],
            ['befriedigend', 'grade-3'],
            ['ausreichend', 'grade-4'],
            ['mangelhaft', 'grade-5'],
            ['ungenügend', 'grade-6'],
        ];

        // get the notes
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setLanguageUid($survey->getLanguageUid());
        $querySettings->setRespectStoragePage(false);
        $this->resultRepository->setDefaultQuerySettings($querySettings);
        $results = $this->resultRepository->findBySurveyAndLang($survey->getUid(),$survey->getLanguageUid());
        $this->view->assign('results', $results);
//        DebuggerUtility::var_dump($results);
//        exit;

        foreach ($results as $result) {
            $notesObj = $result->getNotes();
            $x = 1;
            foreach ($notesObj as $note) {
                $i = $x++;
                if($note->getNote()) {
                    if($note->getType() == 'Matrix') {
                        $matrixQuestion = $this->matrixquestionRepository->findByUid($note->getQuestion());
                        $matrixnotes[$i]['notes'][] = $note->getNote();
                        $matrixnotes[$i]['answer'] = $matrixQuestion->getUid();
                    } else {
                        $notes[$i]['notes'][] = $note->getNote();
                        $notes[$i]['question'] = $note->getQuestion();
                    }
                }
            }
        }

        if(is_array($matrixnotes)) {
            foreach ($matrixnotes as $uid => $matrixnoteArr) {
                $matrixnotesArray[$matrixnoteArr['answer']] = implode(', ',   $matrixnoteArr['notes']);
            }
            ksort($matrixnotesArray);
        }

        if(is_array($notes)) {
            foreach ($notes as $uid => $noteArr) {
                $notesArray[$noteArr['question']] = implode(', ',   $noteArr['notes']);
            }
            ksort($notesArray);
        }

        $x = 1;
        foreach ($survey->getQuestions() as $question) {
            if($question->getLanguageUid() == $survey->getLanguageUid()) {
                $i = $x++;
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $question->getTitle());
                $spreadsheet->getActiveSheet()->getStyle('A'.$i.':B'.$i)->applyFromArray($styleQuestion);
                if($question->getType() != 'Matrix') {
                    $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue('C'.$i, $notesArray[$question->getUid()]);
                }
                $y = $i + 1;

                if($question->getType() == 'Grades') {
                    $grades = $gradesData;
                    foreach ($grades as $index => $grade) {
                        $percents = $this->getPercents($grades[$index][1], $question->getUid(), $question->getType(),$survey->getLanguageUid());
                        $grades[$index][1] = $percents.'%';
                    }
                    $spreadsheet->getActiveSheet()
                        ->fromArray(
                            $grades,  // The data to set
                            NULL,        // Array values with this value will not be set
                            'A'.($i + 1)         // Top left coordinate of the worksheet range where
                        );
                    $x = $i + count($gradesData) + 1;
                } elseif($question->getType() == 'Matrix') {
                    foreach ($question->getMatrixquestions() as $matrixquestion) {
                        $s = $y++;
                        $spreadsheet->setActiveSheetIndex(0)
                            ->setCellValue('A'.$s, $matrixquestion->getTitle());
                        $spreadsheet->getActiveSheet()->getStyle('A'.$s.':B'.$s)->applyFromArray($styleMatrixquestion);
                        $spreadsheet->setActiveSheetIndex(0)
                            ->setCellValue('C'.$s, $matrixnotesArray[$matrixquestion->getUid()]);
                        foreach ($question->getAnswers() as $answer) {
                            $s = $y++;
                            $percents = $this->getPercents($answer->getUid(), $matrixquestion->getUid(),'Matrix',$survey->getLanguageUid());
                            $spreadsheet->setActiveSheetIndex(0)
                                ->setCellValue('A'.$s, $answer->getTitle().' ')
                                ->setCellValue('B'.$s, $percents.'%');
                        }
                        $x = $s + 1;
                    }
                    $x = $s + 1;
                } else {
                    foreach ($question->getAnswers() as $answer) {
                        $s = $y++;
                        $percents = $this->getPercents($answer->getUid(),$question->getUid(),$question->getType(),$survey->getLanguageUid());
                        $spreadsheet->setActiveSheetIndex(0)
                            ->setCellValue('A'.$s, $answer->getTitle())
                            ->setCellValue('B'.$s, $percents.'%');
                    }
                    $x = $s + 1;
                }
            }
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth('100');
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth('100');

        // Rename worksheet
        // max length == 31
        $croppedTitle = mb_strimwidth($survey->getTitle(), 0, 31, "...");
        $spreadsheet->getActiveSheet()->setTitle( $croppedTitle );

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'. str_replace(' ','-',$survey->getTitle()) .'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }


    /**
     * @param $answerUid
     * @param $questionUid
     * @param $type
     * @param $lang
     * @return float
     */
    function getPercents($answerUid, $questionUid, $type, $lang)
    {
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectSysLanguage(FALSE);
        $querySettings->setRespectStoragePage(FALSE);
        $this->resultAnswerRepository->setDefaultQuerySettings($querySettings);

        $total = $this->resultRepository->countAllInvitationsByLang($lang);
        if ($type == 'Grades') {
            $type = 'Radios';
        }
        $countResultAnswers = $this->resultAnswerRepository->findByAnswerAndQuestion($answerUid, $questionUid, $type)->count();
        $res = ($countResultAnswers * 100) / $total;
        $percent = round($res, 0, PHP_ROUND_HALF_ODD);
        return $percent;
    }


    /**
     * action show
     *
     * @param \EXOTEC\Questionaire\Domain\Model\Survey $survey
     * @return void
     */
    public function showAction(\EXOTEC\Questionaire\Domain\Model\Survey $survey)
    {

        $gradesData = [
            ['sehr gut', 'grade-1'],
            ['gut', 'grade-2'],
            ['befriedigend', 'grade-3'],
            ['ausreichend', 'grade-4'],
            ['mangelhaft', 'grade-5'],
            ['ungenügend', 'grade-6'],
        ];

        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setLanguageUid($survey->getLanguageUid());
        $querySettings->setRespectStoragePage(false);
        $this->resultRepository->setDefaultQuerySettings($querySettings);

        $results = $this->resultRepository->findBySurveyAndLang($survey->getUid(),$survey->getLanguageUid());
        $this->view->assign('results', $results);

        foreach ($results as $result) {
            $notesObj = $result->getNotes();
            $x = 1;
            foreach ($notesObj as $note) {
                $i = $x++;
                if($note->getNote()) {
                    if($note->getType() == 'Matrix') {
                        $matrixQuestion = $this->matrixquestionRepository->findByUid($note->getQuestion());
                        $matrixnotes[$i]['notes'][] = $note->getNote();
                        $matrixnotes[$i]['answer'] = $matrixQuestion->getUid();
                    } else {
                        $notes[$i]['notes'][] = $note->getNote();
                        $notes[$i]['question'] = $note->getQuestion();
                    }
                } else {
                    unset($notes[$i]);
                }
            }
        }


        if( is_array($matrixnotes) ) {
            foreach ($matrixnotes as $uid => $matrixnoteArr) {
                $matrixnotesArray[$matrixnoteArr['answer']] = implode(', ',   $matrixnoteArr['notes']);
            }
            ksort($matrixnotesArray);
        }

        if( is_array($notes) ) {
            foreach ($notes as $uid => $note) {
                $notesArray[$note['question']] = implode(', ',   $note['notes']);
            }
            if( is_array($notesArray)) {
                ksort($notesArray);
            }
        }

        /** @var Question $question */
        foreach ($survey->getQuestions() as $question) {
            if($question->getLanguageUid() == $survey->getLanguageUid()) {
                if($question->getType() == 'Grades') {
                    $grades = $gradesData;
                    $x = 1;
                    foreach ($grades as $index => $grade) {
                        $uid = $x++;
                        $percents = $this->getPercents($grades[$index][1], $question->getUid(), $question->getType(),$survey->getLanguageUid());
                        $gradesObj['uid'] = $uid;
                        $gradesObj['points'] = $uid;
                        $gradesObj['title'] =$grades[$index][0];
                        $gradesObj['percents'] = $percents;

                        $cntInvitations = $this->resultRepository->countAllInvitationsByLang($survey->getLanguageUid());
                        $gradesObj['total'] = round($percents / 100 * $cntInvitations);
                        $gradesArr[$question->getUid()][] = $gradesObj;
                    }

                } elseif($question->getType() == 'Matrix') {
                    foreach ($question->getMatrixquestions() as $matrixquestion) {
                        foreach ($question->getAnswers() as $answer) {
                            $percents = $this->getPercents($answer->getUid(), $matrixquestion->getUid(),'Matrix',$survey->getLanguageUid());
                            $matrixAnswers[$matrixquestion->getUid()][$answer->getUid()]['percents'] = $percents;
//                            // JS CHARTS
//                            $cntInvitations = $this->resultRepository->countAllInvitationsByLang($survey->getLanguageUid());
//                            $matrixAnswers[$matrixquestion->getUid()][$question->getUid()]['total'] = round($percents / 100 * $cntInvitations);
//                            $matrixAnswers[$matrixquestion->getUid()][$question->getUid()]['title'] = $answer->getTitle();
//                            $matrixAnswers[$matrixquestion->getUid()][$question->getUid()]['question'] = $question->getTitle();
                        }
                    }

                } else {
                    foreach ($question->getAnswers() as $answer) {
                        $percents = $this->getPercents($answer->getUid(),$question->getUid(),$question->getType(),$survey->getLanguageUid());
                        $answer->setPercents($percents);
                        $total = $this->resultAnswerRepository->findByAnswerAndQuestion($answer->getUid(), $question->getUid(), $question->getType())->count();
                        $answer->setTotal($total);
                    }
                }
            }
        }



        $invitations = $this->invitationRepository->findAllByLang($survey->getLanguageUid());
        $this->view->assign('invitations', $invitations);
        $this->view->assign('langLabel', 'DEUTSCH');
        if($survey->getLanguageUid() == 1) {
            $this->view->assign('langLabel', 'ENGLISCH');
        }

        $this->view->assign('survey', $survey);
        $this->view->assign('grades', $gradesArr);
        $this->view->assign('matrixAnswers', $matrixAnswers);



        $this->view->assign('notes', $notesArray);
        $this->view->assign('matrixnotes', $matrixnotesArray);
    }


    /**
     * action result
     *
     * @param \EXOTEC\Questionaire\Domain\Model\Result $result
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation $result
     * @return void
     */
    public function resultAction(\EXOTEC\Questionaire\Domain\Model\Result $result)
    {

        $renderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class);
        $renderer->addJsFile('EXT:questionaire/Resources/Public/JavaScript/tx_questionaire_pi1_replace_markers.js', 'text/javascript', false, false, '', true,  '|', false, '');

        $surveyUid = $result->getSurvey();
        $invitation = $this->invitationRepository->findByUid($result->getInvitation());

        /** @var Survey $survey */
        $survey = $this->surveyRepository->findByUid($surveyUid);
        $questions = $survey->getQuestions();

        // build an array with resultAnswer uid's which we can compare in the foreach's
        /** @var ResultAnswer $resultAnswer */
        foreach ($result->getAnswers() as $resultAnswer) {
            $resultAnswers[$resultAnswer->getUid()] = $resultAnswer->getAnswer();

            if($resultAnswer->getType() == 'Matrix') {
                $matrixAnswers[$resultAnswer->getQuestion()] = $resultAnswer->getAnswer();
            }
        }

        /** @var Question $answer */
        foreach ($questions as $question) {
            $type = $question->getType();
            foreach ($question->getAnswers() as $answer) {
                if($type != 'Matrix') {
                    $answers[] = $answer;
                }
            }
            foreach ($question->getMatrixquestions() as $matrixquestion) {
                $matrixquestions[$matrixquestion->getUid()] = $matrixAnswers[$matrixquestion->getUid()];
            }
        }

        /** @var Answer $answer */
        foreach ($answers as $key => $answer) {
            $uid = $answer->getUid();
            if( in_array($uid, $resultAnswers) ) {
                $answer->setChecked(1);
            }

        }


        // set grades
        $grades = GeneralUtility::trimExplode(',', $this->settings['grades']);
        foreach ($grades as $key => $value) {
            $gradesArr[$key]['uid'] = $value;
            $gradesArr[$key]['points'] = $value;
            $gradesArr[$key]['title'] = LocalizationUtility::translate('label.grade.'.$value,'Questionaire');
            if( in_array('grade-'.$value, $resultAnswers) ) {
                $gradesArr[$key]['checked'] = 1;
            }
        }

        // build an array with the resultNotes which we can put in the textareas
        /** @var ResultNote $note */
        foreach ($result->getNotes() as $note) {
            if($note->getType() == 'Matrix') {
                $matrixquestion = $this->matrixquestionRepository->findByUid($note->getQuestion());
                $matrixnotes[$matrixquestion->getUid()] = $note->getNote();
            } else {
                $question = $this->questionRepository->findByUid($note->getQuestion());
                $notes[$question->getUid()] = $note->getNote();
            }
        }

        /** @var Question $question */
        foreach ($survey->getQuestions() as $index => $question) {
            if($question->getType() == 'Radios' && $question->getHtmlElement() == 'Select') {
                /** @var ResultAnswer $answer */
                foreach ($result->getAnswers() as $answer) {
                    if( $answer->getType() == 'Radios' && $answer->getQuestion() == $question->getUid() ) {
                        $options = SurveyController::getSelectOptions($question->getAnswers(), $answer->getAnswer());
                    }
                }
                $this->view->assign('selectOptions', $options);
            }
        }

        $this->view->assign('notes', $notes);
        $this->view->assign('result', $result);
        $this->view->assign('matrixnotes', $matrixnotes);
        $this->view->assign('matrixanswers', $matrixquestions);
        $this->view->assign('grades', $gradesArr);
        $this->view->assign('survey', $survey);
        $this->view->assign('invitation', $invitation);
        $this->view->assign('disabled', 1);

    }

}
