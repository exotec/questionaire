<?php
declare(strict_types = 1);

return [
    \EXOTEC\Questionaire\Domain\Model\FrontendUser::class => [
        'tableName' => 'fe_users',
    ],
    \EXOTEC\Questionaire\Domain\Model\Invitation::class => [
        'tableName' => 'tx_questionaire_domain_model_invitation',
        'properties' => [
            'sysLanguageUid' => [
                'fieldName' => 'sys_language_uid'
            ],
            'crdate' => [
                'fieldName' => 'crdate'
            ],
        ],
    ],
];
