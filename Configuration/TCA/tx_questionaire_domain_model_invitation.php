<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_invitations',
        'label' => 'fe_user',
        'label_alt' => 'survey',
        'label_alt_force' => 1,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'fe_user, survey, result, survey_started, survey_started_date, survey_completed, survey_completed_date',
        'iconfile' => 'EXT:questionaire/Resources/Public/Icons/tx_questionaire_domain_model_invitation.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, crdate, fe_user, survey, result, survey_started, survey_started_date, survey_completed, survey_completed_date',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, crdate, fe_user, survey, result, survey_started, survey_started_date, survey_completed, survey_completed_date, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_questionaire_domain_model_invitation',
                'foreign_table_where' => 'AND tx_questionaire_domain_model_invitation.pid=###CURRENT_PID### AND tx_questionaire_domain_model_invitation.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],

        'crdate' => [
            'exclude' => true,
            'label' => 'crdate',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],

        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'fe_user' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_invitation.fe_user',
            'config' => [
                'type' => 'group',
                'size' => 1,
                'internal_type' => 'db',
                'allowed' => 'fe_users',
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => true,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
        ],

        'result' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_invitation.result',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],

        'survey' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_invitation.survey',
            'config' => [
                'type' => 'group',
                'size' => 1,
                'internal_type' => 'db',
                'allowed' => 'tx_questionaire_domain_model_survey',
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => true,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
        ],

        'survey_started' => [
            'exclude' => true,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_invitation.survey_started',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'yes'
                    ]
                ],
            ],
        ],

        'survey_started_date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_invitation.survey_started_date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],


        'survey_completed' => [
            'exclude' => true,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_invitation.survey_completed',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'yes'
                    ]
                ],
            ],
        ],

        'survey_completed_date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_invitation.survey_completed_date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
    
    ],
];
