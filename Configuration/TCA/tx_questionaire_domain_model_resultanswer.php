<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_resultanswer',
        'label' => 'question',
        'label_alt' => 'answer',
        'label_alt_force' => 1,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'question,answer',
        'iconfile' => 'EXT:questionaire/Resources/Public/Icons/tx_questionaire_domain_model_resultanswer.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, type, question, answer',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, type, question, answer, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_questionaire_domain_model_resultanswer',
                'foreign_table_where' => 'AND tx_questionaire_domain_model_resultanswer.pid=###CURRENT_PID### AND tx_questionaire_domain_model_resultanswer.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'question' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_resultanswer.question',
            'config' => [
                'type' => 'input',
                'size' => 4,
            ]
        ],

//        'question' => [
//            'exclude' => false,
//            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_resultanswer.question',
//            'config' => [
//                'type' => 'inline',
//                'foreign_table' => 'tx_questionaire_domain_model_question',
//                'foreign_field' => 'uid',
//                'foreign_sortby' => 'sorting',
//                'maxitems' => 1,
//                'appearance' => [
//                    'collapseAll' => 1,
//                    'levelLinksPosition' => 'top',
//                    'showSynchronizationLink' => 1,
//                    'showPossibleLocalizationRecords' => 1,
//                    'useSortable' => 1,
//                    'showAllLocalizationLink' => 1
//                ],
//            ],
//
//        ],

        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_resultanswer.questiontype',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'answer' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_resultanswer.answer',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],

        'type' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_question.type',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
    
        'result' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
