<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_question',
        'label' => 'title',
        'label_alt' => 'matrixquestions',
        'label_alt_force' => 1,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,description,type,answers,matrixquestions',
        'iconfile' => 'EXT:questionaire/Resources/Public/Icons/tx_questionaire_domain_model_question.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, min_answers, max_answers, type, html_element, answers, matrixquestions',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, type, html_element, min_answers, max_answers, answers, matrixquestions, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_questionaire_domain_model_question',
                'foreign_table_where' => 'AND tx_questionaire_domain_model_question.pid=###CURRENT_PID### AND tx_questionaire_domain_model_question.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],


        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_question.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'description' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_survey.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 3,
                'eval' => 'trim',
            ],

        ],
        'type' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_question.type',
            'onChange' => 'reload',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- bitte wählen --', 0],
                    ['-- Mehrfachauswahl --', 'Checkboxes'],
                    ['-- Einfachauswahl --', 'Radios'],
                    ['-- Schulnoten --', 'Grades'],
                    ['-- Matrix --', 'Matrix'],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'html_element' => [
            'displayCond' => 'FIELD:type:=:Radios',
            'label' => 'HTML Element',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 'Radios',
                'items' => [
                    ['Radios', 'Radios'],
                    ['Select', 'Select']
                ]
            ],
        ],
        'min_answers' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_question.min_answers',
            'displayCond' => 'FIELD:type:=:Checkboxes',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 1,
            ]
        ],
        'max_answers' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_question.max_answers',
            'displayCond' => 'FIELD:type:=:Checkboxes',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => '99',
            ]
        ],
        'answers' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_question.answers',
            'displayCond' => [
                'OR' => [
                    'FIELD:type:=:Checkboxes',
                    'FIELD:type:=:Radios',
                    'FIELD:type:=:Matrix',
                ],
            ],
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_questionaire_domain_model_answer',
                'foreign_field' => 'question',
                'foreign_sortby' => 'sorting',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'useSortable' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
        'matrixquestions' => [
            'exclude' => false,
            'label' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_domain_model_question.matrixquestions',
            'displayCond' => 'FIELD:type:=:Matrix',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_questionaire_domain_model_matrixquestion',
                'foreign_field' => 'question',
                'foreign_sortby' => 'sorting',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'useSortable' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
    
        'survey' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
