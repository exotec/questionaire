<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'EXOTEC.Questionaire',
            'Pi1',
            'Questionaire'
        );


        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
            'EXOTEC.Questionaire',
            'web', // Make module a submodule of 'web'
            'mod1', // Submodule key
            '', // Position
            [
                \EXOTEC\Questionaire\Controller\BackendController::class => 'index, overview, show, csv, result',
            ],
            [
                'access' => 'user,group',
                'icon' => 'EXT:questionaire/Resources/Public/Icons/user_mod_mod1.svg',
                'labels' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_mod1.xlf',
            ]
        );


        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('questionaire', 'Configuration/TypoScript', 'Questionaire');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:questionaire/Configuration/PageTS/TSconfig.txt">');

//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_questionaire_domain_model_survey', 'EXT:questionaire/Resources/Private/Language/locallang_csh_tx_questionaire_domain_model_survey.xlf');
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_questionaire_domain_model_survey');
//
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_questionaire_domain_model_question', 'EXT:questionaire/Resources/Private/Language/locallang_csh_tx_questionaire_domain_model_question.xlf');
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_questionaire_domain_model_question');
//
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_questionaire_domain_model_answer', 'EXT:questionaire/Resources/Private/Language/locallang_csh_tx_questionaire_domain_model_answer.xlf');
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_questionaire_domain_model_answer');
//
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_questionaire_domain_model_matrixquestion', 'EXT:questionaire/Resources/Private/Language/locallang_csh_tx_questionaire_domain_model_matrixquestion.xlf');
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_questionaire_domain_model_matrixquestion');
//
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_questionaire_domain_model_result', 'EXT:questionaire/Resources/Private/Language/locallang_csh_tx_questionaire_domain_model_result.xlf');
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_questionaire_domain_model_result');
//
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_questionaire_domain_model_resultanswer', 'EXT:questionaire/Resources/Private/Language/locallang_csh_tx_questionaire_domain_model_resultanswer.xlf');
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_questionaire_domain_model_resultanswer');
//
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_questionaire_domain_model_resultnote', 'EXT:questionaire/Resources/Private/Language/locallang_csh_tx_questionaire_domain_model_resultnote.xlf');
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_questionaire_domain_model_resultnote');

    }
);
