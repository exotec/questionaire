# Documentation and Demo

[v9 - https://questionaire.exotec.de/v9/public/](https://questionaire.exotec.de/v9/public/)

[v8 - https://questionaire.exotec.de/v9/public/](https://questionaire.exotec.de/v8/public/)


# Site Configuration

**YAML**

    ...
    
    routeEnhancers:
      Questionaire:
        type: Extbase
        limitToPages:
          - 88
        extension: Questionaire
        plugin: Pi1
        routes:
          -
            routePath: '/{survey}'
            _controller: 'Survey::show'
            _arguments:
              survey_slug: survey
        requirements:
          survey_slug: '^[a-zA-Z0-9].*$'
        aspects:
          survey_slug:
            type: PersistedAliasMapper
            tableName: tx_questionaire_domain_model_survey
            routeFieldName: slug