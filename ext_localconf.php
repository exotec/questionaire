<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'EXOTEC.Questionaire',
            'Pi1',
            [
                \EXOTEC\Questionaire\Controller\SurveyController::class => 'list, show',
                \EXOTEC\Questionaire\Controller\ResultController::class => 'list, show, new, create'
            ],
            // non-cacheable actions
            [
                \EXOTEC\Questionaire\Controller\SurveyController::class => 'list, show',
                \EXOTEC\Questionaire\Controller\ResultController::class => 'create, show'
            ]
        );


        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        pi1 {
                            iconIdentifier = questionaire-plugin-pi1
                            title = LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_pi1.name
                            description = LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:tx_questionaire_pi1.description
                            tt_content_defValues {
                                CType = list
                                list_type = questionaire_pi1
                            }
                        }
                    }
                    show = *
                }
           }'
        );

        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        $iconRegistry->registerIcon(
            'questionaire-plugin-pi1',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:questionaire/Resources/Public/Icons/user_plugin_pi1.svg']
        );

        // Add invite task
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\EXOTEC\Questionaire\Task\Invite::class] = array(
            'extension' => 'questionaire',
            'title' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:task.invite.name',
            'description' => 'LLL:EXT:questionaire/Resources/Private/Language/locallang_db.xlf:task.invite.description',
            'additionalFields' => \EXOTEC\Questionaire\Task\InviteAdditionalFieldProvider::class
        );

    }
);
